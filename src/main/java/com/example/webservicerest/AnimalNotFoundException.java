package com.example.webservicerest;

public class AnimalNotFoundException extends Exception {
    public AnimalNotFoundException(String id) {
        super(id);
    }
}
