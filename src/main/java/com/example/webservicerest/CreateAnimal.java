package com.example.webservicerest;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binomialName;
}
