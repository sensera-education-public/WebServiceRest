package com.example.webservicerest;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
